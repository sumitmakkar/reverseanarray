#include<iostream>
#include<vector>

using namespace std;

class Engine
{
private:
	   vector<int> arr;
	   int         rotationTimes;
    
	   void displayArr()
	   {
           int len = (int)arr.size();
           for(int i = 0 ; i < len ; i++)
           {
               cout<<arr[i]<<" ";
           }
           cout<<endl;
       }
    
	   void rotateArrayWithinRange(int start , int end)
	   {
           while(start < end)
           {
               int temp   = arr[start];
               arr[start] = arr[end];
               arr[end]   = temp;
               start++;
               end--;
           }
       }
    
public:
	   Engine(vector<int> a , int rT)
	   {
           arr           = a;
           rotationTimes = rT;
       }
    
	   void rotateArray()
	   {
           int len   = (int)arr.size();
           int start = (rotationTimes % len) - 1;
           rotateArrayWithinRange(0 , start);
           rotateArrayWithinRange(start+1 , len-1);
           rotateArrayWithinRange(0 , len-1);
           displayArr();
       }
};

int main()
{
    vector<int> arr;
    for(int i = 1 ; i <= 5 ; i++)
    {
        arr.push_back(i);
    }
    Engine e = Engine(arr , 4);
    e.rotateArray();
    return 0;
}
